#!/usr/bin/env bash

#@(#)=======================================================================
#@(#)    Name         : borg-bck.sh
#@(#)    Description  : description
#@(#)    Auteur       : Christophe Chaudier & Bettioui Mohammed-Amine
#@(#)    Commentaires : Made with 💚
#@(#)======================================================================
#-----------------------------------------------------------------------------#
#                                                                             #
# Copyright 2014-2019 Chaudier Christophe http://www.cchaudier.fr/            #
#                                                                             #
# This file is part of ShellFactory.                                          #
#                                                                             #
# ShellFactory is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# ShellFactory is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with ShellFactory. If not, see <http://www.gnu.org/licenses/>.        #
#                                                                             #
#-----------------------------------------------------------------------------#

_init() {
  borg_repo=${2}

  echo ${borg_repo} | grep "ssh:"
  if [ $? -ne 0 ]; then
    mkdir -p ${borg_repo} || _fail "Error: the repository [${borg_repo}] can't be created !"
  fi

  borg check ${borg_repo} 2>/dev/null || borg init --encryption=none ${borg_repo} \
    || _fail "Error: the repository [${borg_repo}] can't be created !"
  echo "The repository [${borg_repo}] was created."
}

_processing_file() {

  echo "Processing file : ${filename}"

  [ -e ${filename} ] || _fail "Error: the file [${filename}] doesn't exist !"
  [ -s ${filename} ] || _fail "Error: the file [${filename}] is empty !"

  line_number=0
  line_in_error=0

  while read line; do
    line_number=$((${line_number} + 1))
    source_dir=$(echo ${line} | cut -d ';' -f 1)
    borg_repo=$(echo ${line} | cut -d ';' -f 2)
    echo "${action_name} : ${line_number} [${line}]"
    if [ "${action}" = "init" ]; then
      borg-bck.sh ${action} ${borg_repo}
    else
      borg-bck.sh ${action} ${source_dir} ${borg_repo}
    fi

    if [ ${?} -eq 0 ]; then
      echo "${file_success_msg}"
    else
      line_in_error=$(expr ${line_in_error} + 1)
      echo "${file_error_msg}"
    fi
  done < ${filename}
  line_success=$(expr ${line_number} - ${line_in_error})

  echo "
  #####################################
  ${action_name} Report :

  Number of lines in success : ${line_success}
  Number of lines in error : ${line_in_error}
  #####################################
  "
  [ ${line_in_error} -eq 0 ] || _fail "Error: the file contained errors and ${action_name} is not complete !"

}

_fail(){
  echo $*
  exit 1
}

_usage() {
  echo "Usage :

  NAME :
        borg-bck.sh

  DESCRIPTION :
        borg-bck.sh is a deduplicating backup script which makes it possible to create backups

  SYNOPSIS :
        borg-bck.sh [ACTION]...

  ACTION :
        init (option) <path>
          Initialize a new backup repository

        path :
          - local path example : /tmp/repository
          - ssh path example : ssh://user@servername/path

        option:
          -f : init repository from a csv file

          format csv file example:

          source_dir_path1;destination_dir_path1
          source_dir_path2;destination_dir_path2
          source_dir_path3;destination_dir_path3

        help
          show help page of this script

        create <source> <destination>
          This command creates a backup archive

          destination:
            - local destination example : /tmp/borg_repository
            - ssh destination example : ssh://user@servername/path

          option:
            -f : init repository from a csv file

            format csv file example:

            source_dir_path1;destination_dir_path1
            source_dir_path2;destination_dir_path2
            source_dir_path3;destination_dir_path3

        restore <source> <destination>
          This command restore a backup

          source:
            - local source example : /tmp/source
            - ssh source example : ssh://ssh://user@servername/path

  "
  exit ${1}
}

_create() {
  name_archive_backup="$(date +%Y%m%d_%H%M%S)"

  [ -d "${source_dir}" ] || _fail "Error: Source directory : ${source_dir} not exist !"
  borg check ${borg_repo} || _fail "Error: Repository ${borg_repo} does not exist !"
  borg create ${borg_repo}::${name_archive_backup} ${source_dir} || _fail "Error: Can not create the backup ! Please check the information entered."
  echo "The backup ${name_archive_backup} was created."
}

_restore() {
  borg_repo="${2}"
  destination_directory="${3}"

  if [ -f "${destination_directory}" ]; then
    _fail "Error: it is a regular file !"
  fi

  mkdir -p ${destination_directory} || _fail  "Error: The destination directory can't be created !"

  if [ -z "$(ls -A ${destination_directory})" ]; then
    borg check ${borg_repo} || _fail "Error: The borg repository does not exist !"
    last_archive_name=$(borg list ${borg_repo} | tail -1 | cut -c1-15)
    borg info ${borg_repo}::${last_archive_name} || _fail "Error: No archive was found !"
    cd ${destination_directory}
    list_of_archive=$(borg extract --list ${borg_repo}::${last_archive_name} 2>&1) || _fail "Error: Restoration failed !"

    files_not_restored=""
    for file in ${list_of_archive}; do
      [ -e ${file} ] || files_not_restored=${files_not_restored}$(echo "\n ${file}")
    done

    if [ -n ${files_not_restored} ]; then
      _fail "/!\ Warning : Restoration finish but some files have not been restored :

      ### the files not restored : ###
      ${files_not_restored}
      "
    else
    echo "Successful restoration."
    fi
  else
    _fail "Error: The directory already contains files !"
  fi
}

#MAIN#
action=${1}
case ${action} in
  init)
    if [ $# -eq 2 ]; then
      _init $*
    elif [ $# -eq 3 ] && [ ${2} = "-f" ]; then
      filename=${3}
      file_success_msg="Initialization success."
      file_error_msg="Error: Initialization failed !"
      action_name="Initialization"
      _processing_file
    else
      _usage 1
    fi
  ;;
  create)
    if [ $# -eq 3 ] && [ ${2} = "-f" ]; then
      filename=$3
      file_success_msg="Creation of backup success."
      file_error_msg="Error: creation of backup failed !"
      action_name="Creation"
      _processing_file
    elif [ $# -eq 3 ]; then
      source_dir="${2}"
      borg_repo="${3}"
      _create
    else
      _usage 1
    fi
  ;;
  restore)
    if [ $# -eq 3 ]; then
      _restore $*
    else
      _usage 1
    fi
  ;;
  help) _usage 0 ;;
  *) _usage 1 ;;
esac
