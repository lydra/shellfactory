#!/usr/bin/env bats
#-----------------------------------------------------------------------------#
#                                                                             #
# Copyright 2014-2019 Chaudier Christophe http://www.cchaudier.fr/            #
#                                                                             #
# This file is part of ShellFactory.                                          #
#                                                                             #
# ShellFactory is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# ShellFactory is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with ShellFactory. If not, see <http://www.gnu.org/licenses/>.        #
#                                                                             #
#-----------------------------------------------------------------------------#

load borg-bck_helper

@test "Test ssh to borg_server" {
  ssh -oStrictHostKeyChecking=no ${ssh_info} true
}

########################## INIT ACTION ##########################

@test "init -> if the script as more than 2 args then failed with usage message" {
  run ${script_to_test} init ${borg_repo_good} error
  [ ${status} -ne 0 ]
}

@test "init -> if the script as more than 2 arguments then script don't create repo" {
  run ${script_to_test} init ${borg_repo_good} error
  ! borg check ${borg_repo_good}
}

@test "init -> if we can't create the repo then failed with error message" {
  run ${script_to_test} init ${borg_repo_bad}
  [ ${status} -ne 0 ]
  ! borg check ${borg_repo_bad}
  echo ${output} | grep "Error: the repository \[${borg_repo_bad}\] can't be created !"
}

@test "init -> if script ok then borg repo is created with success message" {
  _test_init_good ${borg_repo_good}
}

@test "init-ssh -> if script ok then borg repo is created with success message" {
  _test_init_good ${borg_repo_good_ssh}
}

@test "init -> if borg repo exist then skip init" {
  borg init --encryption=none ${borg_repo_good}
  run ${script_to_test} init ${borg_repo_good}
  [ ${status} -eq 0 ]
}

@test "init -> if the directory not exist then creates it with success message" {
  run ${script_to_test} init ${borg_repo_no_exist}
  [ ${status} -eq 0 ]
  echo ${output} | grep "The repository \[${borg_repo_no_exist}\] was created"
}

@test "init-ssh -> if the directory not exist and script can't creates it then fail with error message" {
  run ${script_to_test} init ${borg_repo_no_exist_ssh}
  [ ${status} -ne 0 ]
  echo ${output} | grep "Error: the repository \[${borg_repo_no_exist_ssh}\] can't be created !"
}

########################## INIT FROM FILE ACTION ##########################

@test "init-file -> if the file not exist then script failed with error message" {
  _test_from_file_no_exist init
}

@test "init-file -> if the file exist and empty then failed with error message" {
  _test_from_file_empty init
}

@test "init-file -> if one of the lines is wrong then initializes the correct lines with result message" {
  _create_file_good
  run ${script_to_test} init -f ${file_csv_contain_error}
  [ ${status} -ne 0 ]
  echo ${output} | grep "Number of lines in error : 2"
  _test_number_expected "Error: Initialization failed !" 2
  echo ${output} | grep "Error: the file contained errors and Initialization is not complete !"
}

@test "init-file -> if all lines have 2 fields and initialization is ok then print success message" {
  _create_file_good
  run ${script_to_test} init -f ${file_csv_good}
  [ ${status} -eq 0 ]
  echo ${output} | grep "Number of lines in success : 5"
  _test_number_expected "Initialization success." 5
}

########################## HELP ACTION ##########################

@test "help -> if argument not exist then print usage" {
  run ${script_to_test} argumenterror
  [ ${status} -ne 0 ]
  [ "${lines[0]}" = "${usage_first_line}" ]
}

@test "help -> if argument is help then print usage" {
  run ${script_to_test} help
  [ ${status} -eq 0 ]
  [ "${lines[0]}" = "${usage_first_line}" ]
}

########################## CREATE ACTION ##########################

@test "create -> if source_dir not exist then failed with error message" {
  run ${script_to_test} create ${source_dir_bad} ${borg_repo_bad}
  [ ${status} -ne 0 ]
  [ "${lines[0]}" = "Error: Source directory : ${source_dir_bad} not exist !" ]
}

@test "create -> if borg_repo not exist then failed with error message" {
  _test_create_borg_repo_not_exist ${borg_repo_bad}
}

@test "create-ssh -> if borg_repo ssh not exist then failed with error message" {
  _test_create_borg_repo_not_exist ${borg_repo_bad_ssh}
}

@test "create -> if source_dir and borg_repo exist then print success message" {
  _test_backup_good ${borg_repo_good}
}

@test "create-ssh -> if source_dir and borg_repo ssh exist then print success message" {
  _test_backup_good "${borg_repo_good_ssh}_${RANDOM}"
}

@test "create -> if the script as more than 3 args then failed and print usage" {
  run ${script_to_test} create ${source_dir_good} ${borg_repo_good} error
  [ ${status} -ne 0 ]
  [ "${lines[0]}" = "${usage_first_line}" ]
}

########################## CREATE FROM FILE ACTION ##########################

@test "create-file -> if the file not exist then failed with error message" {
  _test_from_file_no_exist create
}

@test "create-file -> if the file exist and empty then failed with error message" {
  touch ${file_csv_empty}
  _test_from_file_empty create
}

@test "create-file -> if one of the lines is wrong then create backup for all correct lines and print result message" {
  _init_file_good
  run ${script_to_test} create -f ${file_csv_contain_error}
  [ ${status} -ne 0 ]
  echo ${output} | grep "Number of lines in error : 2"
  _test_number_expected "Error: creation of backup failed !" 2
  echo ${output} | grep "Error: the file contained errors and Creation is not complete !"
}

@test "create-file -> if all lines have 2 fields and backups are ok then print success message" {
  _init_file_good
  run ${script_to_test} create -f ${file_csv_good}
  [ ${status} -eq 0 ]
  echo ${output} | grep "Number of lines in success : 5"
  _test_number_expected "Creation of backup success." 5
}

########################## RESTORE ACTION ##########################

@test "restore-ssh -> if borg_repo ssh not exist then failed with error message" {
  _test_restore_bad ${borg_repo_bad_ssh} ${destination_dir_good} "Error: The borg repository does not exist !"
}

@test "restore -> if borg archive not exist then failed with error message" {
  run borg init --encryption=none ${borg_repo_good}
  _test_restore_bad ${borg_repo_good} ${destination_dir_good} "Error: No archive was found !"
}

@test "restore-ssh -> if borg archive not exist then failed with error message" {
  run borg init --encryption=none ${borg_repo_good_ssh_restore}
  _test_restore_bad ${borg_repo_good_ssh_restore} ${destination_dir_good} "Error: No archive was found !"
}

@test "restore -> if borg_repo not exist then failed with error message" {
  _test_restore_bad ${borg_repo_bad} ${destination_dir_good} "Error: The borg repository does not exist !"
}

@test "restore-ssh -> if borg_repo not exist then failed with error message" {
  _test_restore_bad ${borg_repo_bad_ssh} ${destination_dir_good} "Error: The borg repository does not exist !"
}

@test "restore -> if can't restore then failed with error message" {
  _creation_borg_repo_and_archive ${borg_repo_good}
  _test_restore_bad ${borg_repo_good} ${destination_dir_bad} "Error: The destination directory can't be created !"
}

@test "restore -> if the script as more than 3 args failed with usage message" {
  run ${script_to_test} restore ${source_dir_good} ${destination_dir_good} error
  [ ${status} -ne 0 ]
  [ "${lines[0]}" = "${usage_first_line}" ]
}

@test "restore -> if destination_dir_good exist and not empty then failed with error message" {
  mkdir ${destination_dir_good}
  touch ${destination_dir_good}/file_in_backup.txt
  _creation_borg_repo_and_archive ${borg_repo_good}
  _test_restore_bad ${borg_repo_good} ${destination_dir_good} "Error: The directory already contains files !"
}

@test "restore -> if destination_dir is a file then failed with error message" {
  touch ${destination_dir_is_file}
  _creation_borg_repo_and_archive ${borg_repo_good}
  _test_restore_bad ${borg_repo_good} ${destination_dir_is_file} "Error: it is a regular file !"
}

@test "restore -> if destination_dir_good don't exist then restore the backup in it with success message" {
  _test_restore_good ${borg_repo_good}
}

@test "restore -> if destination_dir_good exist then restore the backup in it with success message" {
  mkdir ${destination_dir_good}
  _test_restore_good ${borg_repo_good}
}

@test "restore -> if restore is ok then print success message" {
  _test_restore_good ${borg_repo_good}
}

@test "restore-ssh -> if restore is ok then print success message" {
  _test_restore_good ${borg_repo_good_ssh_restore}
}
