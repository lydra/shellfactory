#Tests for borg-bck.sh
setup() {

  ssh_info="borg@borg_server"

  script_to_test="${BATS_TEST_DIRNAME}/../bin/borg-bck.sh"
  export PATH=${BATS_TEST_DIRNAME}/../bin:$PATH

  borg_repo_no_exist="${BATS_TMPDIR}/thereisnothing/here"
  borg_repo_no_exist_ssh="ssh://${ssh_info}/backup/bats/thereisnothing/here"
  borg_repo_bad="/tmp_ro/borg_repo_bad"
  borg_repo_bad_ssh="ssh://${ssh_info}/backup/bats/borg_repo_bad"
  borg_repo_good="${BATS_TMPDIR}/borg_repo_good"
  borg_repo_good_ssh="ssh://${ssh_info}/backup/bats/borg_repo_good"
  borg_repo_good_ssh_restore="ssh://${ssh_info}/backup/bats/borg_repo_good_${RANDOM}"

  #init from file action
  file_csv_bad="/tmp_ro/file_bad.csv"
  file_csv_empty="${BATS_TEST_DIRNAME}/files/file_csv_empty.csv"
  file_csv_contain_error="${BATS_TEST_DIRNAME}/files/file_contain_error.csv"
  file_csv_good="${BATS_TEST_DIRNAME}/files/file_good.csv"

  #help action
  usage_first_line="Usage :"

  #create action
  source_dir_bad="/tmp_ro/source_dir_bad"
  source_dir_good="${BATS_TMPDIR}/source_dir_good"

  #restore action
  destination_dir_bad="/tmp_ro/destination_dir_bad"
  destination_dir_good="${BATS_TMPDIR}/destination_dir_good"
  destination_dir_is_file="${BATS_TMPDIR}/destination_dir_is_file.txt"

  #from files
  source_good=$(echo ${BATS_TMPDIR}/source_good/{1..5})
  dest_good=$(echo ${BATS_TMPDIR}/dest_good/{1..5})
}

teardown() {
  rm -Rf ${borg_repo_good}
  rm -Rf ${destination_dir_good}
  rm -Rf ${source_dir_good}
  rm -Rf ${archive_exist}
  rm -Rf ${source_good}
  rm -Rf ${dest_good}
  rm -Rf ${borg_repo_no_exist}
}

_create_file_good() {
  mkdir -p ${source_good}
  mkdir -p ${dest_good}
}

_init_file_good() {
  _create_file_good
  for borg_repo in ${dest_good}; do
    borg init --encryption="none" ${borg_repo}
  done
}

_creation_borg_repo_and_archive() {
  borg_repo=${1}
  mkdir -p ${source_dir_good}
  touch ${source_dir_good}/file_in_backup.txt
  borg init --encryption=none ${borg_repo}
  borg check ${borg_repo}
  borg create ${borg_repo}::"$(date +%Y%m%d_%H%M%S)" ${source_dir_good}
}

_test_init_good() {
  borg_repo=${1}
  run ${script_to_test} init ${borg_repo}
  echo ${output} | grep "The repository \[${borg_repo}\] was created"
  echo ${output} | grep -v "Error: Repository" | grep -v "does not exist !"
  [ ${status} -eq 0 ]
  borg check ${borg_repo}
}

_test_create_borg_repo_not_exist() {
  borg_repo=${1}
  mkdir -p ${source_dir_good}
  run ${script_to_test} create ${source_dir_good} ${borg_repo}
  [ ${status} -ne 0 ]
  echo ${output} | grep "Error: Repository ${borg_repo} does not exist !"
}

_test_backup_good() {
  mkdir -p ${source_dir_good}
  borg init --encryption=none ${1}
  run ${script_to_test} create ${source_dir_good} ${1}
  [ ${status} -eq 0 ]
  echo ${output} | grep "The backup"|grep "was created."
}

_test_restore_bad() {
  borg_repo=${1}
  destination_dir=${2}
  error_msg=${3}
  run ${script_to_test} restore ${borg_repo} ${destination_dir}
  [ "${status}" -ne 0 ]
  echo ${output} | grep "${error_msg}"
}

_test_restore_good() {
  _creation_borg_repo_and_archive ${1}
  run ${script_to_test} restore ${1} ${destination_dir_good}
  [ ${status} -eq 0 ]
  [ -e ${destination_dir_good}/${source_dir_good}/file_in_backup.txt ]
  echo ${output} | grep "Successful restoration."
}

_test_number_expected() {
  sentence=${1}
  number_expected=${2}
  number_of_error_result=0
  for index in "${!lines[@]}"; do
    if [ "${lines[index]}" = "${sentence}" ]; then
      number_of_error_result=$((${number_of_error_result}+1))
    fi
  done

  if [ ${number_of_error_result} -eq ${number_expected} ]; then
    return 0
  else
    return 1
  fi
}

_test_from_file_no_exist() {
  run ${script_to_test} ${1} -f ${file_csv_bad}
  [ ${status} -ne 0 ]
  echo ${output} | grep "Error: the file \[$file_csv_bad\] doesn't exist !"
}

_test_from_file_empty() {
  run ${script_to_test} ${1} -f ${file_csv_empty}
  [ ${status} -ne 0 ]
  echo ${output} | grep "Error: the file \[$file_csv_empty\] is empty !"
}
